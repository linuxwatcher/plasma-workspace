# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Kishore G <kishore96@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-04 02:12+0000\n"
"PO-Revision-Date: 2022-07-05 19:49+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.2\n"

#: autostartmodel.cpp:322
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "\"%1\" ஒரு முழுமையான முகவரி இல்லை."

#: autostartmodel.cpp:325
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1\" என்பது இல்லை."

#: autostartmodel.cpp:328
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" ஒரு கோப்பு இல்லை."

#: autostartmodel.cpp:331
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\"-ஐ படிக்க முடியவில்லை."

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr "இயக்கவல்லதாக்கு"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr "வெளியேறும்போது இயக்கப்பட, '%1' எனும் கோப்பு இயக்கவல்லதாக இருக்கவேண்டும்."

#: package/contents/ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr "நுழையும்போது இயக்கப்பட, '%1' எனும் கோப்பு இயக்கவல்லதாக இருக்கவேண்டும்."

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Properties"
msgstr "பண்புகள்"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Remove"
msgstr "நீக்கு"

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr "செயலிகள்"

#: package/contents/ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr "நுழைவு சிறுநிரல்கள்"

#: package/contents/ui/main.qml:118
#, kde-format
msgid "Pre-startup Scripts"
msgstr "துவக்கத்திற்கு முன் இயக்க வேண்டிய சிறுநிரல்கள்"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr "வெளியேற்ற சிறுநிரல்கள்"

#: package/contents/ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr "பயனரால் அமைக்கப்பட்ட சுயதுவக்க நிரல்கள் எதுவும் இல்லை"

#: package/contents/ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr "சிலவற்றை சேர்க்க <interface>சேர்…</interface> பட்டனை அழுத்துங்கள்"

#: package/contents/ui/main.qml:145
#, kde-format
msgid "Choose Login Script"
msgstr "நுழைவு சிறுநிரலைத் தேர்ந்தெடுத்தல்"

#: package/contents/ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr "வெளியேற்ற சிறுநிரலைத் தேர்ந்தெடுத்தல்"

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr "சேர்…"

#: package/contents/ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr "செயலியை சேர்…"

#: package/contents/ui/main.qml:203
#, kde-format
msgid "Add Login Script…"
msgstr "நுழைவு சிறுநிரலை சேர்…"

#: package/contents/ui/main.qml:209
#, kde-format
msgid "Add Logout Script…"
msgstr "வெளியேற்ற சிறுநிரலை சேர்…"

#~ msgid "Autostart"
#~ msgstr "சுயதுவக்கம்"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "அமர்வு சுயதுவக்க கையாளி கட்டுப்பாடு கூறு"

#~ msgid "Maintainer"
#~ msgstr "பராமரிப்பாளர்"

#~ msgid "Add..."
#~ msgstr "சேர்..."

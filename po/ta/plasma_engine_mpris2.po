# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Kishore G <kishore96@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-15 00:20+0000\n"
"PO-Revision-Date: 2021-07-15 21:38+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#: multiplexedservice.cpp:68
#, kde-format
msgctxt "Name for global shortcuts category"
msgid "Media Controller"
msgstr "ஊடக கட்டுப்படுத்தி"

#: multiplexedservice.cpp:70
#, kde-format
msgid "Play/Pause media playback"
msgstr "ஊடகத்தை இயக்கு/இடைநிறுத்து"

#: multiplexedservice.cpp:88
#, kde-format
msgid "Media playback next"
msgstr "ஊடக இயக்கத்தில் அடுத்தது"

#: multiplexedservice.cpp:97
#, kde-format
msgid "Media playback previous"
msgstr "ஊடக இயக்கத்தில் முந்தையது"

#: multiplexedservice.cpp:106
#, kde-format
msgid "Stop media playback"
msgstr "ஊடக இயக்கத்தை நிறுத்து"

#: multiplexedservice.cpp:115
#, kde-format
msgid "Pause media playback"
msgstr "ஊடக இயக்கத்தை இடைநிறுத்து"

#: multiplexedservice.cpp:124
#, kde-format
msgid "Play media playback"
msgstr "ஊடக இயக்கத்தை இயக்கு"

#: multiplexedservice.cpp:133
#, kde-format
msgid "Media volume up"
msgstr "ஊடக ஒலி அளவை கூட்டு"

#: multiplexedservice.cpp:142
#, kde-format
msgid "Media volume down"
msgstr "ஊடக ஒலி அளவை குறை"

#: playeractionjob.cpp:169
#, kde-format
msgid "The media player '%1' cannot perform the action '%2'."
msgstr "'%1' ஊடக இயக்கியால், '%2' என்ற செயலை செய்ய முடியாது."

#: playeractionjob.cpp:171
#, kde-format
msgid "Attempting to perform the action '%1' failed with the message '%2'."
msgstr "'%1' என்ற செயல், '%2' என்ற தகவலுடன் தோல்வியடைந்தது."

#: playeractionjob.cpp:173
#, kde-format
msgid "The argument '%1' for the action '%2' is missing or of the wrong type."
msgstr ""
"'%2' என்ற செயலுக்கான '%1' என்ற தரு மதிப்பு தரப்படவில்லை, அல்லது தவறான வகையானது."

#: playeractionjob.cpp:175
#, kde-format
msgid "The operation '%1' is unknown."
msgstr "'%1' என்ற செயல், தெரியாதது."

#: playeractionjob.cpp:177
#, kde-format
msgid "Unknown error."
msgstr "தெரியாத சிக்கல்."

# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2018, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-11 00:46+0000\n"
"PO-Revision-Date: 2021-05-27 18:38+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: appstreamrunner.cpp:34
#, kde-format
msgid "Looks for non-installed components according to :q:"
msgstr "Söker efter icke-installerade komponenter enligt :q:"

#: appstreamrunner.cpp:133
#, kde-format
msgid "Get %1…"
msgstr "Hämta %1…"
